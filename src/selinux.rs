use libc::{statfs, statvfs, ST_RDONLY};
use std::fs::{metadata, read};
use std::ffi::CString;
use std::mem::MaybeUninit;

const SELINUXCONFIG: &str = "/etc/selinux/config";

pub fn is_selinux_enabled() -> bool {
    if ! verify_mnt() {
	return false;
    }
    metadata(SELINUXCONFIG).is_ok()
}

fn verify_mnt() -> bool {
    let dir = CString::new("/sys/fs/selinux").unwrap();
    let mut ufs = MaybeUninit::<statfs>::uninit();
    let fs = unsafe {
	// FIXME: EINTR  The call was interrupted by a signal;
        statfs(dir.as_ptr(), ufs.as_mut_ptr());
        ufs.assume_init()
    };
    if fs.f_type != 0xf97cff8c {
	return false;
    }

    let mut uvfs = MaybeUninit::<statvfs>::uninit();
    let vfs = unsafe {
        statvfs(dir.as_ptr(), uvfs.as_mut_ptr());
        uvfs.assume_init()
    };

    vfs.f_flag & ST_RDONLY != ST_RDONLY
}

//        int getcon(char **context);

// int getcon_raw(char **context);
pub fn getcon() -> Result<String, i32> {
    let attr_file = format!("/proc/thread-self/attr/current");
    match read(attr_file) {
	Ok(current) =>
	    Ok(String::from_utf8(current).unwrap().trim_matches(char::from(0).to_string())),
	       //current.parse().unwrap().trim_end()),
	Err(_) => Err(-1)
    }
}

// int getprevcon(char **context);
// int getprevcon_raw(char **context);
// int getpidcon(pid_t pid, char **context);
// int getpidcon_raw(pid_t pid, char **context);
// int getpeercon(int fd, char **context);
// int getpeercon_raw(int fd, char **context);
// void freecon(char *con);
// void freeconary(char **con);
// int setcon(char *context);
// int setcon_raw(char *context);

#[cfg(test)]
mod tests {
    use libc::{statfs, statvfs, ST_RDONLY};
    use std::ffi::CString;
    use std::mem::MaybeUninit;

    use super::*;

    #[test]
    fn test_statfs() {
        // https://docs.rs/libc/0.2.63/libc/fn.statfs.html
        // https://docs.rs/libc/0.2.63/libc/struct.statfs.html
        // statfs(path: *const c_char, buf: *mut statfs) -> c_int
        let dir = CString::new("/sys/fs/selinux").unwrap();
        let mut ufs = MaybeUninit::<statfs>::uninit();
        let fs = unsafe {
            statfs(dir.as_ptr(), ufs.as_mut_ptr());
            ufs.assume_init()
        };
        assert_eq!(fs.f_type, 0xf97cff8c);

	let mut uvfs = MaybeUninit::<statvfs>::uninit();
        let vfs = unsafe {
            statvfs(dir.as_ptr(), uvfs.as_mut_ptr());
            uvfs.assume_init()
        };

	assert_ne!(vfs.f_flag & ST_RDONLY, ST_RDONLY);
    }

    #[test]
    fn test_is_selinux_enabled() {
	assert_eq!(is_selinux_enabled(), true);
    }

    #[test]
    fn test_getcon() {
	let current = getcon();
	assert!(current.is_ok());
	assert_eq!(current.unwrap(), "unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023");
    }
}
