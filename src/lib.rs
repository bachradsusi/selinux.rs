mod selinux;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_selinux_enabled() {
	assert_eq!(selinux::is_selinux_enabled(), true);
    }
}
